### Products
=========

Admintool to handle and manage products and groups with prices.


#### Requirements

* [LEPTON CMS][1], Version see precheck.php
 

#### Installation

* download latest [products.zip][2] installation archive
* in CMS backend select the file from "Add-ons" -> "Modules" -> "Install module"

#### Notice

After installing addon go to admintools, enter Products and start to work. <br />
For further details please see [readme file][3].


[1]: https://lepton-cms.org "LEPTON CMS"
[2]: http://www.lepton-cms.com/lepador/admintools/products.php
[3]: https://cms-lab.com/_documentation/products/readme.php

