<?php

/**
 * @module          Products
 * @author          cms-lab
 * @copyright       2021-2021 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
class products extends LEPTON_abstract
{
	public $all_products = array();
	public $active_products = array();
	public $all_groups = array();
	public $active_groups = array();	
	public $database = 0;
	public $admin = 0;
	public $addon_color = 'blue';
	public $action_url = ADMIN_URL . '/admintools/tool.php?tool=products';
	public $action = LEPTON_URL . '/modules/products/';
		
	
	public static $instance;	
	
	public function initialize() 
	{
		$this->database = LEPTON_database::getInstance();		
		$this->admin = LEPTON_admin::getInstance();	
		$this->init_tool();		
	}
	
	public function init_tool( $sToolname = '' )
	{
		//get all productss
		$this->all_products = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_products ORDER BY id ASC",
			true,
			$this->all_products,
			true
		);		

		//get active productss
		$this->active_products = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_products WHERE active = 1 ORDER BY id ASC",
			true,
			$this->active_products,
			true
		);

		//get all groups
		$this->all_groups = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_products_groups " ,
			true,
			$this->all_groups,
			true
		);		

		//get active productss
		$this->active_groups = array();
		$this->database->execute_query(
			"SELECT * FROM ".TABLE_PREFIX."mod_products_groups WHERE active = 1 " ,
			true,
			$this->active_groups,
			true
		);			
	}	
	

	public function list_products()
	{
		// data for twig template engine	
		$data = array(
			'oPR'			=> $this,	
			'readme_link'	=> "https://cms-lab.com/_documentation/products/readme.php",			
			'leptoken'		=> get_leptoken()
			);

		/**	
		 *	get the template-engine.
		 */
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('products');
			
		echo $oTwig->render( 
			"@products/list.lte",	//	template-filename
			$data					//	template-data
		);
	
	}	

	public function show_info() 
	{
		// create links	
		$support_link = "<a href='#'>NO Live-Support / FAQ</a>";
		$readme_link = "<a href='https://cms-lab.com/_documentation/products/readme.php' class='info' target='_blank'>Readme</a>";		

		// data for twig template engine	
		$data = array(
			'oPR'			=> $this,
			'readme_link'	=> $readme_link,		
			'SUPPORT'		=> $support_link,		
			'image_url'		=> 'https://cms-lab.com/_documentation/media/products/products.jpg'
			);

		/**	
		 *	get the template-engine.
		 */
		$oTwig = lib_twig_box::getInstance();
		$oTwig->registerModule('products');
			
		echo $oTwig->render( 
			"@products/info.lte",	//	template-filename
			$data					//	template-data
		);		
		
	}	
	
	public function edit_products($id) 
	{
		if (is_numeric($id)  && $id == -1 )
		{
			LEPTON_handle::register( "random_string" );
			$last_id = $this->database->get_one("SELECT MAX(id) FROM ".TABLE_PREFIX."mod_products");
			$code = (100000 + 1 + $last_id);
			$fields = array(
				'group_id' => 1,
				'code' => $code,
				'amount' => '-1',
				'currency' => '1',
				'unit'	=> 'Kg',				
				'title' => 'Neuer Eintrag',
				'start' => date("Y-m-d"),
				'end' => date("Y-m-d")
			);
			
			$result = $this->database->build_and_execute (
				"INSERT",
				TABLE_PREFIX."mod_products",
				$fields
			);

			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');
		}		
		elseif (is_numeric($id) && $id > 0 )
		{
			$products = intval($id);
			$current_products = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_products WHERE id = ".$products ,
				true,
				$current_products,
				false
			);		
				
			// data for twig template engine	
			$data = array(
				'oPR'			=> $this,
				'current_products'=> $current_products,
				'leptoken'		=> get_leptoken()
			);

			/**	
			 *	get the template-engine.
			 */
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('products');
					
			echo $oTwig->render( 
				"@products/edit.lte",	//	template-filename
				$data					//	template-data
			);				
		}
	}	

	
	public function save_products($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$products_id = intval($id);
//die(LEPTON_tools::display($_POST, 'pre','ui blue message'));			
			$request = LEPTON_request::getInstance();
			$all_names = array (
				'group_id'	=> array ('type' => 'int', 'default' => "1"),
				'code'		=> array ('type' => 'string_chars', 'default' => ""),
				'amount'	=> array ('type' => 'decimal', 'default' => -1.00),
				'currency'	=> array ('type' => 'int', 'default' => "1"),
				'unit'		=> array ('type' => 'string_chars', 'default' => ""),				
				'title'		=> array ('type' => 'string_chars', 'default' => ""),
				'start'		=> array ('type' => 'date', 'default' => ""),
				'end'		=> array ('type' => 'date', 'default' => ""),
				'link_title'=> array ('type' => 'string_clean', 'default' => ""),
				'ext_url'	=> array ('type' => 'string_clean', 'default' => ""),				
				'active'	=> array ('type' => 'int', 'default' => "1")
			);		

			$all_values = $request->testPostValues($all_names);			
			$table = TABLE_PREFIX."mod_products";
			$this->database->build_and_execute( 'UPDATE', $table, $all_values,'id = '.$products_id);
			
			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');			
		}		
	}


	public function activate_products($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$products_id = intval($id);
			$handle_active = $this->database->get_one("SELECT active FROM ".TABLE_PREFIX."mod_products WHERE id = ".$products_id);
			
			if( $handle_active == 1)
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_products SET active = 0 WHERE id = ".$products_id);
			}
			else
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_products SET active = 1 WHERE id = ".$products_id);
			}
			
			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');			
		}		
	}	

	
	public function delete_products($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$to_delete = intval($id);
			
			$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_products WHERE id = ".$to_delete);					
			$this->admin->print_success($this->language['delete_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');			
		}		
	}
	

	public function edit_group($id) 
	{
		if (is_numeric($id)  && $id == -1 )
		{
			$fields = array(
				'group_name' => 'New Group'
			);
			
			$result = $this->database->build_and_execute (
				"INSERT",
				TABLE_PREFIX."mod_products_groups",
				$fields
			);

			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');
		}		
		elseif (is_numeric($id) && $id > 0 )
		{
			$group = intval($id);
			$current_group = array();
			$this->database->execute_query(
				"SELECT * FROM ".TABLE_PREFIX."mod_products_groups WHERE group_id = ".$group ,
				true,
				$current_group,
				false
			);		
				
			// data for twig template engine	
			$data = array(
				'oPR'			=> $this,
				'current_group'=> $current_group,
				'leptoken'		=> get_leptoken()
			);

			/**	
			 *	get the template-engine.
			 */
			$oTwig = lib_twig_box::getInstance();
			$oTwig->registerModule('products');
					
			echo $oTwig->render( 
				"@products/edit_group.lte",	//	template-filename
				$data					//	template-data
			);				
		}
	}

	public function save_group($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$group_id = intval($id);
			
			$request = LEPTON_request::getInstance();
			$all_names = array (
				'group_name'=> array ('type' => 'string_chars', 'default' => ""),
				'active'	=> array ('type' => 'int', 'default' => "1")
			);		

			$all_values = $request->testPostValues($all_names);			
			$table = TABLE_PREFIX."mod_products_groups";
			$this->database->build_and_execute( 'UPDATE', $table, $all_values,'group_id = '.$group_id);
			
			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');			
		}		
	}

	
	public function activate_group($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$group_id = intval($id);
			$handle_active = $this->database->get_one("SELECT active FROM ".TABLE_PREFIX."mod_products_groups WHERE group_id = ".$group_id);
//die(LEPTON_tools::display($handle_active,'pre','ui message'));			
			if( $handle_active == 1)
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_products_groups SET active = 0 WHERE group_id = ".$group_id);
			}
			else
			{
				$this->database->simple_query("UPDATE ".TABLE_PREFIX."mod_products_groups SET active = 1 WHERE group_id = ".$group_id);
			}
			
			$this->admin->print_success($this->language['save_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');			
		}		
	}
	
	public function delete_group($id) 
	{
		if (is_numeric($id)  && $id > 0 )			
		{
			$to_delete = intval($id);
			
			$this->database->simple_query("DELETE FROM ".TABLE_PREFIX."mod_products_groups WHERE group_id = ".$to_delete);					
			$this->admin->print_success($this->language['delete_ok'], ADMIN_URL.'/admintools/tool.php?tool=products');			
		}		
	}			
}