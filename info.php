<?php

/**
 * @module          Products
 * @author          cms-lab
 * @copyright       2021-2021 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$module_directory	= 'products';
$module_name		= 'Products';
$module_function	= 'tool';
$module_version		= '1.0.0';
$module_platform	= '5.x';
$module_delete		=  true;
$module_author		= 'cms-lab';
$module_home		= 'https://cms-lab.com';
$module_guid		= '56a5abd5-ff48-4466-a00b-43784d3efaba';
$module_license		= '<a href="https://cms-lab.com/_documentation/products/license.php" target="_blank">Custom License</a>';
$module_license_terms	= '<a href="https://cms-lab.com/_documentation/products/license.php" target="_blank">see License</a>';
$module_description	= 'Admintool to handle and manage products and groups with prices';

?>