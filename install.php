<?php

/**
 * @module          Products
 * @author          cms-lab
 * @copyright       2021-2021 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php


// install table
$table_fields = "
    `id` int(11) NOT NULL AUTO_INCREMENT,
	`group_id` int(11) NOT NULL DEFAULT '-1',
    `code` varchar(32) NOT NULL DEFAULT '-1',
    `currency` int(1) NOT NULL DEFAULT '1',
	`amount` decimal(15,2) NOT NULL DEFAULT '-1.00',
	`unit` varchar(32) NOT NULL DEFAULT 'Kg',	
    `title` varchar(256) NOT NULL DEFAULT 'none',
    `start` date DEFAULT '1971-01-01',
    `end` date DEFAULT '1971-01-01',
    `link_title` varchar(256) NOT NULL DEFAULT '',
    `ext_url` varchar(256) NOT NULL DEFAULT '',	
    `active` int(1) NOT NULL DEFAULT '1',
    PRIMARY KEY ( `id` )
";
LEPTON_handle::install_table("mod_products", $table_fields);

$table_fields = "
    `group_id` int(11) NOT NULL AUTO_INCREMENT,
    `group_name` varchar(256) NOT NULL DEFAULT 'none',
    `active` int(1) NOT NULL DEFAULT '1',
    PRIMARY KEY ( `group_id` )
";
LEPTON_handle::install_table("mod_products_groups", $table_fields);

$field_values="
	(1,'none',1)
";
LEPTON_handle::insert_values("mod_products_groups", $field_values);

?>