<?php

/**
 * @module          Products
 * @author          cms-lab
 * @copyright       2021-2021 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */

// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {	
	include(LEPTON_PATH.'/framework/class.secure.php'); 
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.'/framework/class.secure.php')) { 
		include($root.'/framework/class.secure.php'); 
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include class.secure.php

$MOD_PRODUCTS = array(
	'action'	    => "Aktion",
	'add'	    	=> "Eintrag hinzufügen",
	'all_courses'  	=> "Alle Seminare",
	'all_groups'  	=> "Alle Gruppen",
	'currency1'	    => "EUR",
	'currency2'	    => "US $",	
	'delete_ok'     => "Datensatz erfolgreich gelöscht",
	'duplicate'     => "Kopieren",
	'edit'	        => "Bearbeiten",
	'error'	        => "FEHLER",
	'ext_url'	    => "Externe URL",
	'header1'	    => "ID",
	'header2'	    => "Titel",
	'header3'	    => "Aktiv/Inaktiv",
	'header4'	    => "Code",
	'header5'	    => "Währung",
	'header6'	    => "Löschen",
	'header7'	    => "Start",
	'header8'	    => "Ende",
	'header9'	    => "Preis",
	'header10'	    => "Name",
	'header11'	    => "Gruppe",
	'help'	    	=> "Hilfe-Seite",
	'info'	        => "Addon Info",
	'link_title'	=> "Link-Titel",	
	'save_ok'	    => "Daten erfolgreich gespeichert",
	'to_delete'	    => "wirklich löschen",
	'unit'		    => "Einheit",	
	'want_delete'	=> "Wollen Sie den Datensatz",

);

?>