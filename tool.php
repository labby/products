<?php

/**
 * @module          Products
 * @author          cms-lab
 * @copyright       2021-2021 cms-lab
 * @link            https://cms-lab.com
 * @license         GNU General Public License 3 (see info.php)
 * @license_terms   see license
 *
 */
 
// include class.secure.php to protect this file and the whole CMS!
if (defined('LEPTON_PATH')) {   
   include(LEPTON_PATH.'/framework/class.secure.php');
} else {
   $oneback = "../";
   $root = $oneback;
   $level = 1;
   while (($level < 10) && (!file_exists($root.'/framework/class.secure.php'))) {
      $root .= $oneback;
      $level += 1;
   }
   if (file_exists($root.'/framework/class.secure.php')) {
      include($root.'/framework/class.secure.php');
   } else {
      trigger_error(sprintf("[ <b>%s</b> ] Can't include class.secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
   }
}
// end include class.secure.php

$debug = true;

if (true === $debug) {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}

if(isset ($_GET['tool'])) {
	$toolname = $_GET['tool'];
} else {
	die('[1]');
}

// get instance of functions file
$oPR = products::getInstance();

if(isset ($_GET['tool']) && (empty($_POST)) )  {
	$oPR->list_products();
}
if(isset ($_POST['show_info']) && ($_POST['show_info']== 'show') ) {
	$oPR->show_info();
}
if(isset ($_POST['add_entry']) ) {
	$oPR->edit_products('-1');
}
if(isset ($_POST['edit_products']) ) {
	$oPR->edit_products($_POST['edit_products']);
}
if(isset ($_POST['save_products']) ) {
	$oPR->save_products($_POST['save_products']);
}
if(isset ($_POST['delete_products']) ) {
	$oPR->delete_products($_POST['delete_products']);
}
if(isset ($_POST['activate_products']) ) {
	$oPR->activate_products($_POST['activate_products']);
}
if(isset ($_POST['add_group']) ) {
	$oPR->edit_group('-1');
}
if(isset ($_POST['edit_group']) ) {
	$oPR->edit_group($_POST['edit_group']);
}
if(isset ($_POST['save_group']) ) {
	$oPR->save_group($_POST['save_group']);
}
if(isset ($_POST['delete_group']) ) {
	$oPR->delete_group($_POST['delete_group']);
}
if(isset ($_POST['activate_group']) ) {
	$oPR->activate_group($_POST['activate_group']);
}
$admin->print_footer();
?>